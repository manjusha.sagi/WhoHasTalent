package com.talent;

public abstract class Performance {
	 static int id;
	 String performancetype;
	
	public String getPerformancetype() {
		return performancetype;
	}
	public Performance(int id,String performancetype) {
		this.id=id;
		this.performancetype = performancetype;
		
	}
	public static  int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	abstract void announce();

}
