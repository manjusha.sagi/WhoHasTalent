package com.talent;

public class VocalistPerformance extends Performance {
	
	private int key;
	private int volume;
	public VocalistPerformance(int id, String performancetype, int key, int volume) {
		super(id, performancetype);
		this.key =key;
		this.volume = volume;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) throws IllegalArgumentException {
		System.out.println("In set Volume");
		if(volume<1 && volume >10) {
			throw new IllegalArgumentException("The Volume value should be between 1 and 10");
		}
		else {
		this.volume = volume;
		}
	}



	@Override
	public void announce() {
		System.out.println("I sing in the key of " +key +" at the volume " +volume);
		
	}

}
