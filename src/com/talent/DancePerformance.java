package com.talent;

public class DancePerformance extends Performance {
	
	public DancePerformance(int id, String performancetype, String style) {
		super(id, performancetype);
		this.style = style;
	}




	private String style;
	private String performancetype;
	
	
	public String getStyle() {
		return style;
	}
	
	


	@Override
	void announce() {
		System.out.println(style + " - " + id + " - " +performancetype);
	}


	


	
}
