package com.talent.testing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.talent.Performance;
import com.talent.PerformerPerformance;

public class PerformerPerformanceTest {

	private Performance pp;

	@Before
	public void setUp() throws Exception {
		pp = new PerformerPerformance(3, "Performer");
	}

	@Test
	public void test() {
		String expected = "Performer";
		String actual = pp.getPerformancetype();
		assertEquals(expected,actual);
	}

}
