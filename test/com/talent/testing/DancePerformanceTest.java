package com.talent.testing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.talent.DancePerformance;
import com.talent.Performance;

public class DancePerformanceTest {



	private DancePerformance p1;

	@Before
	public void setUp() throws Exception {
		p1 = new DancePerformance(2, "Dancer", "tap");
	}

	@Test
	public void testStyle() {
		String expected = "tap";
		String actual = p1.getStyle();
		assertEquals(expected,actual);
	}

}
