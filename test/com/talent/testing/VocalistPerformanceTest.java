package com.talent.testing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.talent.VocalistPerformance;

public class VocalistPerformanceTest {

	VocalistPerformance obj;
	
	private VocalistPerformance Pf;

	@Before
	public void setUp() throws Exception {
		
		Pf = new VocalistPerformance(1,"Vocalist", 2,3);
	}

	@Test
	public void testingKey() {
		int actual = 2;
		Pf = new VocalistPerformance(1,"Vocalist", 2,3);
		int expected = Pf.getKey();
		assertEquals(actual,expected);
	}
	
	@Test
	public void testingVolume() {
		int actual = 3;
		
		int expected = Pf.getVolume();
		assertEquals(actual,expected);
	}
	
	/*@Rule
	
	public ExpectedException exception = ExpectedException.none();
	
	@Test
	public void volumeRangeTest() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("The Volume value should be between 1 and 10");
		Pf.setVolume(9);
	}*/
}